﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using BopDoctorsContactForms.Models.Entidades;
namespace BopDoctorsContactForms.Models.Metodos
{
    public class DBDOCTORS
    {
        private string archivo;
        public DBDOCTORS(string _archivo)
        {            
            archivo = File.ReadAllText(_archivo);
        }
        public DBDOCTORS()
        {
        }

        public void Escribir(Archivos archivo)
        {
            File.WriteAllText(archivo.ruta, archivo.datos);
        }

        public List<T > Lista<T>()
        {
            List<T> result = JsonConvert.DeserializeObject<List<T>>(archivo);
            return result;
        }

        public List<T> Lista<T>(string f)
        {
            string _f=File.ReadAllText(f);
            List<T> result = JsonConvert.DeserializeObject<List<T>>(_f);
            return result;
        }

        public bool Existencia(Archivos file)
        {
            return File.Exists(file.ruta);
        }

        public void GuardarFile(string ruta, string name)
        {
            File.Copy(ruta, name);
        }
    }
}
