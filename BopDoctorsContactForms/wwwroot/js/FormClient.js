﻿classDatos = new AccesosDatos();
empresa = {
    id: $("#lblempresa").val()
}
classDatos.Ajax(
    "/Home/Empresa",
    JSON.stringify(empresa),
    function (datos) {
        $("#imgemp").attr("src", datos.logo);
        $("#lblemp").text(datos.name)
    })


control = {
    idpais: $("#lblpais").val(),
    idempresa: $("#lblempresa").val(),
    nomc: $("#lblcod").val()
};
classDatos.Ajax(
    "/Home/ListaControl",
    JSON.stringify(control),
    function (datos) {
        for (x in datos) {
            $("#divadd").append(
                '<div class="col-md-6" > ' +
                '<div class="input-group mb-3">' +
                '<div class="input-group-prepend">' +
                '<span class="input-group-text">' + datos[x].nomc + ' :</span>' +
                '</div>' +
                '<input id="' + datos[x].id + '"  type="' + datos[x].tpc + '" class="form-control"/>' +
                '</div>' +
                '</div>'
            );
        }
        $("[type='date']").kendoDatePicker({
            format: "yyyy-MM-dd",
            dateInput: true
        });
    })

classDatos.Ajax(
    "/Home/ListaDocumentos",
    JSON.stringify(control),
    function (datos) {
        Grilla(datos);
    })

classDatos.Ajax("/Home/ListaPaises", '', function (datos) {
    $("#cmbpais").html("").append("<option value='-1'>Seleccione</option>");
    for (var x in datos) {
        $("#cmbpais").append("<option value='" + datos[x].idpais + "'>" + datos[x].Name + "</option>")
    }
    $("#cmbpais").val($("#lblpais").val())
})

classDatos.Ajax("/Home/ListaEmpresa", '', function (datos) {
    $("#cmbempresa1").html("").append("<option value='-1'>Seleccione</option>");
    for (var x in datos) {
        $("#cmbempresa1").append("<option value='" + datos[x].id + "'>" + datos[x].name + "</option>")
    }
    $("#cmbempresa1").val($("#lblempresa").val())
})

classDatos.Upload("#fileDoc", "/UploadFiles", "Subir Documentos");
classDatos.Ajax(
    "/Home/Cliente",
    JSON.stringify(control),
    function (datos) {
        console.log(datos);
        $("#txtnom").val(datos.nombres)
        $("#txtapel").val(datos.apellidos)
        $("#txtafiliado").val(datos.afiliado)
        $("#txtmail").val(datos.mail)
        $("#txtmovil").val(datos.celular)
        $("#txtuser").val(datos.usuario)
        if (datos.newcontrol != null) {
            for (x in datos.newcontrol) {
                $("#" + datos.newcontrol[x].idcont).val(datos.newcontrol[x].valcontrol)
            }
        }
    })

$("body").on("click", "#btnconfirm", function () {
    var validar = classDatos.ValidarCampos("body .form-control")
    if (validar) {
        classDatos.OpenWindows("#div-confirmacion", "Confirmación", 100, 300);
    } else {
        classDatos.OpenWindows("#div-alert", "Advertencia", 100, 300);
    }
})


$("body").on("click", "#btnclose", function () {
    classDatos.CloseWindows("#div-alert")
})

$("body").on("click", "#btnatras", function () {
    classDatos.LimpiarCampos("#divadd .form-control");
    $("[type='password'], #txtuser").val("");
})

arraycontroles = new Array();
$("body").on("click", "#btnsave", function () {
    $("#divadd .form-control").each(function (key, element) {
        control = {
            idcont: $(element).attr("id"),
            valcontrol: $(element).val()
        }
        arraycontroles.push(control)
    });
    $("#fileDoc").uploadifySettings('scriptData', {
        'id': $("#lblcod").val(),
        'nombres': $("#txtnom").val(),
        'apellidos': $("#txtapel").val(),
        'pais': $("#lblpais").val(),
        'celular': $("#txtmovil").val(),
        'empresa': $("#lblempresa").val(),
        'afiliado': $("#txtafiliado").val(),
        'mail': $("#txtmail").val(),
        'usuario': $("#txtuser").val(),
        'psw': $("#txtpsw").val(),
        'newcontrol': JSON.stringify(arraycontroles)
    });
    classDatos.Ajax(
        "/Home/ListaDocumentos",
        JSON.stringify(control),
        function (datos) {
            Grilla(datos);
        })
    classDatos.CloseWindows("#div-confirmacion");
    classDatos.OpenWindows("#div-mensaje", "Mensaje", 100, 300);
    $("#fileDoc").uploadifyUpload()
})

$("body").on("blur", "#txtconfirmpsw", function () {
    if ($(this).val() != $("#txtpsw").val()) {
        $(this).val("");        
    }
})

$("body").on("click", "#btncancel", function () {
    classDatos.CloseWindows("#div-confirmacion");
})

$("body").on("click", "#btnclosemsj", function () {
    classDatos.CloseWindows("#div-mensaje");
})



function Grilla(data) {
    classDatos.Grilla(
        "#div-grid-doc",
        data,
        [
            {
                field: "filename",
                title: "Archivo"
            },            
            {
                field: "filename",
                title: "Descargar",
                template: function (d) {
                    return '<a href="../../Documentos/'+d.filename+'" target="_black"><img  style="max-width=2em; max-height: 3em;" src="../../images/pdf.ico" /><a/>'
                }
            }
        ]
    )
}